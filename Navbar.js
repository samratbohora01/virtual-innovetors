import React, {useState} from 'react'
import './Navbar.css'
import {FaFacebookSquare, FaInstagramSquare, FaYoutubeSquare } from 'react-icons/fa'
import { GiHamburgerMenu } from 'react-icons/gi'


const Navbar= () => {
    const [showMediaIcons, setShowMediaIcons] = useState(false); 

   
  return (

    
  
    <>

        <nav className='main-nav'>
            <div className='logo'>
                    <img src='#'/>
            </div>
                
            {/*Menu prt */}

            <div className= {showMediaIcons ? "menu-link mobile-menu-link ": "menu-link"}>

                <ul>
                    <li>
                        <a href='#'>Home</a>
                    </li>
                    <li>
                        <a href='#'>About</a>
                    </li>
                    <li>
                        <a href='#'>Services</a>
                    </li>
                    <li>
                        <a href='#'>Our Client</a>
                    </li>
                    <li>
                        <a href='#'>Contact</a>
                    </li>  
                </ul>    
             </div>

             {/* 3rd social media */}

             <div className='social-media'>
                <ul className='social-media-desktop'>
                    <li>
                        <a href='#' target='virtual'>
                            
                        </a>
                    </li>
                    <li>
                        <a href='#' target='virtual'>
                            
                        </a>
                    </li>
                    <li>
                        <a href='#' target='virtual'>
                            
                        </a>
                    </li>
                    
                </ul>

                {/*hamburger menu */}
                 <div className='hamburger-menu'>
                    <a href='#' onClick={() => setShowMediaIcons(!showMediaIcons)}>
                        <GiHamburgerMenu/>
                    </a>
                 </div>

             </div>

        </nav>
    </>
  )
}

export default Navbar

