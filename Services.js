import React from "react";
import './Services.css'
import mobile from '../images/image1.png'
import WebAssetIcon from '@mui/icons-material/WebAsset';
import DesignServicesIcon from '@mui/icons-material/DesignServices';
import PhoneAndroidIcon from '@mui/icons-material/PhoneAndroid';
import SupportIcon from '@mui/icons-material/Support';

function Services(){
    return(
        <>
        <div className="Services">
            <h1>Our Services</h1>

            <div className="service-content">
                <div className="column-container">
                    <div className="item-container-1">
                        <div className="icon">
                             <div className="ic"><WebAssetIcon fontSize='Large'/></div> 
                             <h2>Web Creation</h2>
                             <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                         sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                         </div>
                         
                    </div>
                    <div className="item-container-2">
                      <div className="icon">
                             <div className="ic"><DesignServicesIcon/></div>
                             <h2>Graphic Design</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                         sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                      </div>
                            

                     </div>
                </div>
                <div className="column-container">
                    <div className="item-container-3">
                        <img src={mobile}/>
                    </div>
                </div>
                <div className="column-container">
                    <div className="item-container-4">
                        <div className="icon">
                            <div className="ic"><PhoneAndroidIcon/></div>
                             <h2>Mobile App</h2>
                             <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                             sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                         </div>
                         
                    </div>
                    <div className="item-container-5">
                        <div className="icon">
                             <div className="ic"><SupportIcon/></div>
                             <h2>Support</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                         </div>
                         
                    </div>
                </div>

            </div>
        
        </div>
        </>
    )
}
export default Services
