import React from "react";
import { Parallax } from 'react-parallax'
import Slider from 'react-slick'
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
import './Testimonial.css'
import { Avatar } from "@mui/material";
import  Gadget2 from   '../images/dark.jpg'
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import image1 from '../images/nikhildai.jpg'



 /* previous part*/
 const PreviousBtn = (props) =>{
    console.log(props);
    const {className, onClick } = props;
    return(
        <div className={className} onClick={onClick}>
            <ArrowBackIosIcon style={{color: 'white', fontSize: '30px' }}/>
        </div>
    )
}
const NextBtn = (props) =>{
    const {className, onClick } = props;
    return(
        <div className={className} onClick={onClick}>
            <ArrowForwardIosIcon style={{color: 'white', fontSize: '30px' }}/>
        </div>
    )
}


const Testimonial = () => {
    return(


        <>
         <Parallax strength={300} bgImage={Gadget2}>
          <div className="testomonial" style={{ display: 'flex', justifyContent: 'center', marginTop: 50}}> 
            <div style={{width:'50%', textAlign : 'center'}}>
                <h1 style={{marginBottom: 30, color: '#CAF0F8', fontFamily: 'Sans serif' }}> Our Customer are seeing big result</h1>

         
                <div className='active'>
                 <div className='img'><img src={image1}/>
                <Slider prevArrow={<PreviousBtn/>} nextArrow={<NextBtn/>} dots color={'white'}>
                    <Card img="../images/nikhildai.jpg"/>
                    <Card />
                    <Card/>
                </Slider>
            </div>
        </div>
        </div>
        </div>
        </Parallax>
        
        </>
    );
};

const Card = ({img}) =>{
    return(
      <Parallax strength={600}>
        <div 
        style={ {
            display: 'flex', 
            alignItems: 'center',
             flexDirection: 'column',
             textAlign: 'center'
        }}>
            <Avatar 
            imgProps={{style: {borderRadius:'100%'}}}
            src={img}
            style={{
                display: 'none',
                weight: 190, 
                height: 140,
                border:'1px solid lightgray',
                padding: 7,
                marginBottom: 10, 
                  }}/>
            <p style={{ marginTop: 60, color: '#CAF0F8', fontSize: 20, fontFamily: 'Georgia'}}>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
            tempor incididunt ut labore et dolore magna aliqua."</p>
            <p><span style={ { fontWeight: 500, color: '#CAF0F8', fontFamily: 'Georgia'}}>-Nikhil Upretti</span></p>
            
            

        </div>
    </Parallax>

    )

}
export default Testimonial;
