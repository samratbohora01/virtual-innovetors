import React, {useState} from 'react'
import './Contact.css'
import  PermPhoneMsgIcon from '@mui/icons-material/PermPhoneMsg';
import { CloseIcon  } from '@mui/icons-material/Close'
import  PhoneInTalkIcon  from '@mui/icons-material/PhoneInTalk';
import  RemoveIcon  from '@mui/icons-material/Remove';

function Contact(){
    const [button, setButton] = useState(false);

    const handleClick = () => setButton(!button);

    return(
        <>
           <div className={button ? 'contact-container-invisible' : 'contact-container'} onClick={handleClick}>
          <PermPhoneMsgIcon className='message'/>
        </div>
        <div className={button ? 'contact-container' : 'contact-container-invisible'}>
          <div className='sidemenu'>
            <div className='contact-header'>
              <h4>Contact us</h4> 
              <RemoveIcon onClick={handleClick}/>
              </div>
              <div className='phone'>
                <PhoneInTalkIcon />
                <h3>9861111233</h3>
              </div>
              <div className='contact-midline'>
                <div className='line'></div>
                Or
                <div className='line'></div>
              </div>
              <div className='phone'>
                <PhoneInTalkIcon />
                <h3>014-23232</h3>
              </div>
          </div>
        </div>

        </>

    )
}
export default Contact